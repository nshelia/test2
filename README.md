# Ennagram Frontend

### Installation

Make sure you have Node (LTS) & Yarn (stable) installed on your machine.

-  Navigate to `app` workspace folder
-  Duplicate `.env.example` file and rename it `.env`
-  Add `127.0.0.1 local..ge` in your machine `hosts` file  
-  Run `yarn install`

### Commands 

- `yarn lint:js` - Run ESLint on client
- `yarn dev` - Run the client webpack dev server
- `yarn build:client` - Build development assets for client
