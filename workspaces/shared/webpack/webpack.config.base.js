const path = require('path')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const isDev = process.env.NODE_ENV !== 'production'
const webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = (env) => ({
  mode: isDev ? 'development' : 'production',
  optimization: {
    minimizer: [
      new TerserPlugin({ terserOptions: { output: { comments: false } } }),
      new OptimizeCSSAssetsPlugin({
        cssProcessor: require('cssnano'),
        cssProcessorPluginOptions: {
          preset: ['default', { discardComments: { removeAll: true } }],
        },
        canPrint: true,
      }),
    ],
  },
  output: {
    path: path.resolve(__dirname, `../../../build/${env.APP_TARGET}`),
    filename: '[name].bundle.js',
    publicPath: '/',
  },
  resolve: {
    modules: ['node_modules'],
    alias: { react: require.resolve('react') },
    extensions: ['.js', '.json'],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            plugins: [
              '@babel/plugin-proposal-object-rest-spread',
              ['@babel/plugin-proposal-class-properties', { spec: true }],
            ],
            env: {
              development: {
                plugins: [
                  'react-hot-loader/babel',
                  ['babel-plugin-styled-components', { displayName: true }],
                ],
              },
              production: {
                plugins: [['babel-plugin-styled-components', { displayName: false }]],
              },
            },
            presets: [
              '@babel/preset-env',
              [
                '@babel/preset-react',
                {
                  throwIfNamespace: false,
                },
              ],
            ],
          },
        },
      },
      {
        test: /\.(png|jpg|gif|svg|ico|ttf|woff|woff2|otf|mp3)$/,
        loader: 'file-loader',
        options: {
          name: isDev ? '[name].[ext]' : '[name]-[hash].[ext]',
        },
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader?sourceMap',
          'resolve-url-loader',
          'sass-loader?sourceMap',
          'import-glob-loader',
        ],
      },
      {
        test: /\.css$/,
        use: ['css-loader', 'resolve-url-loader', 'import-glob-loader'],
      },
    ],
  },
  devtool: 'source-map',
  plugins: [
    new webpack.NormalModuleReplacementPlugin(/(.*)-PLATFORM(\.*)/, function (resource) {
      resource.request = resource.request.replace(/-PLATFORM/, `-${env.PLATFORM}`)
    }),
    new webpack.NormalModuleReplacementPlugin(/(.*)-APP_TARGET(\.*)/, function (resource) {
      resource.request = resource.request.replace(/-APP_TARGET/, `-${env.APP_TARGET}`)
    }),
  ],
})
