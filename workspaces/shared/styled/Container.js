import { device } from '@root/shared/helpers/device'
import styled from 'styled-components'

export const StyledContainer = styled.div`
  width: 100%;
  position: relative;
  background: #ffffff;
  @media (max-width: 1024px) {
    padding-left: 0px;
    padding-top: 67px;
  }
  @media (max-width: 768px) {
    padding-top: 32px;
  }
`

export const StyledListWrapper = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  &::-webkit-scrollbar {
    width: 6px;
  }
  &::-webkit-scrollbar-thumb {
    background-color: ${(props) => props.theme.darkGray};
  }
  overflow: auto;
  padding: 16px 0 0 32px;
  ${device.medium} {
    padding: 0;
    height: 318px;
  }
`
