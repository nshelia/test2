import React from 'react'

function Radio() {
  return (
    <svg width='16' height='17' viewBox='0 0 16 17' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M0.992 4.12C0.408 4.344 0 4.936 0 5.6V15.2C0 16.08 0.712 16.8 1.6 16.8H14.4C15.288 16.8 16 16.08 16 15.2V5.6C16 4.712 15.288 4 14.4 4H5.04L11.648 1.328L11.104 0L0.992 4.12ZM4 15.2C2.672 15.2 1.6 14.128 1.6 12.8C1.6 11.472 2.672 10.4 4 10.4C5.328 10.4 6.4 11.472 6.4 12.8C6.4 14.128 5.328 15.2 4 15.2ZM14.4 8.8H12.8V7.2H11.2V8.8H1.6V5.6H14.4V8.8Z'
        fill='#FFFFFF'
      />
    </svg>
  )
}

export default Radio
