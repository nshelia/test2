import React from 'react'

function Movie() {
  return (
    <svg width='16' height='13' viewBox='0 0 16 13' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M12.8 0L14.4 3.2H12L10.4 0H8.8L10.4 3.2H8L6.4 0H4.8L6.4 3.2H4L2.4 0H1.6C0.72 0 0.00799999 0.72 0.00799999 1.6L0 11.2C0 12.08 0.72 12.8 1.6 12.8H14.4C15.28 12.8 16 12.08 16 11.2V0H12.8Z'
        fill='#FFFFFF'
      />
    </svg>
  )
}

export default Movie
