import styled from 'styled-components'

export const StyledButton = styled.button`
  background: none;
  border: none;
  padding: 0 ${(props) => props.theme.spacing.l};
`
