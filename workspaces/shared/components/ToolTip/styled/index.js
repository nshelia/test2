import { device } from '@root/shared/helpers/device'
import styled, { css } from 'styled-components'

export const StyledToolTipWrapper = styled.div`
  display: inline-block;
  position: relative;
  /* width: 100%; */
`

const TOOLTIP_MARGIN = '30px'
const ARROW_SIZE = '6px'

export const StyledToolTipText = styled.div`
  position: absolute;
  text-align: center;
  align-items: center;
  background: ${(props) => props.theme.pureBlack};
  color: ${(props) => props.theme.almostGray};
  left: 50%;
  transform: translateX(-50%);
  padding: 6px;
  font-size: 12px;
  line-height: 1;
  min-width: 70px;
  z-index: 10000;
  white-space: nowrap;
  font-family: 'robotoNusx';
  ${device.medium} {
    font-size: 10px;
  }
  &:before {
    content: ' ';
    left: calc(50% - 6px);
    border: solid transparent;
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    border-width: ${ARROW_SIZE};
    margin-left: calc(${ARROW_SIZE} * -1);
  }
  ${(props) =>
    props.direction === 'top' &&
    css`
      top: ${TOOLTIP_MARGIN};
      &:before {
        top: 100%;
        border-top-color: ${(props) => props.theme.pureBlack};
      }
    `}
  ${(props) =>
    props.direction === 'right' &&
    css`
      left: calc(100% + ${TOOLTIP_MARGIN});
      top: 50%;
      transform: translateX(0) translateY(-50%);
      &:before {
        left: calc(${ARROW_SIZE} * -1);
        top: 50%;
        transform: translateX(0) translateY(-50%);
        border-right-color: ${(props) => props.theme.pureBlack};
      }
    `}

  ${(props) =>
    props.direction === 'bottom' &&
    css`
      bottom: calc(${TOOLTIP_MARGIN} * -1);
      margin-left: ${(props) => (props.left ? '-25px' : props.right ? '20px' : '0')};
      &:before {
        margin-left: ${(props) => (props.left ? '20px' : props.right ? '-20px' : '0')};
        bottom: 100%;
        border-bottom-color: ${(props) => props.theme.pureBlack};
      }
    `}

  ${(props) =>
    props.direction === 'left' &&
    css`
      left: auto;
      right: calc(100% + ${TOOLTIP_MARGIN});
      top: 50%;
      transform: translateX(0) translateY(-50%);
      &:before {
        left: auto;
        right: calc(${ARROW_SIZE} * -2);
        top: 50%;
        transform: translateX(0) translateY(-50%);
        border-left-color: ${(props) => props.theme.pureBlack};
      }
    `}
`
