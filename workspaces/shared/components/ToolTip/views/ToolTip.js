import React, { useState } from 'react'

import { StyledToolTipText, StyledToolTipWrapper } from '../styled'

const ToolTip = (props) => {
  let timeout = null
  const [active, setActive] = useState(false)

  const showTip = () => {
    setActive(true)
  }

  const hideTip = () => {
    setActive(false)
  }
  return (
    <StyledToolTipWrapper onMouseEnter={showTip} onMouseLeave={hideTip}>
      {props.children}
      {active && (
        <StyledToolTipText
          direction={props.direction || 'bottom'}
          left={props.left}
          right={props.right}
        >
          {props.content}
        </StyledToolTipText>
      )}
    </StyledToolTipWrapper>
  )
}

export default ToolTip
