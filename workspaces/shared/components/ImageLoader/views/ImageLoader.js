import noImg from '@root/shared/icons/noImg/noImage.png'
import React, { useState } from 'react'

import { StyledImage } from '../styled'

function ImageLoader({ src }) {
  const [loaded, setLoaded] = useState(false)

  function noImageError(e) {
    e.target.onError = null
    e.target.src = noImg
  }

  return (
    <StyledImage
      onTransitionEnd={(e) => {
        e.stopPropagation()
        e.preventDefault()
      }}
      onLoad={() => setLoaded(true)}
      show={loaded}
      src={src}
      onError={(e) => noImageError(e)}
    />
  )
}

export default ImageLoader
