import React from 'react'

import { StyledBeat } from '../styled'

function Loader() {
  return (
    <StyledBeat>
      <div></div>
      <div></div>
      <div></div>
    </StyledBeat>
  )
}

export default Loader
