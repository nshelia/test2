import styled, { keyframes } from 'styled-components'

export const ballBeat = keyframes`
  50% {
    opacity: .2;
    transform: scale(.75);
  }
  100% {
    opacity: 1;
    transform: scale(1);
  }
`

export const StyledBeat = styled.div`
  position: relative;
  > div {
    position: relative;
    display: inline-block;
    float: none;
    background-color: ${(props) => props.theme.red};
    border: 0 solid ${(props) => props.theme.red};
    width: 15px;
    height: 15px;
    margin: 4px;
    border-radius: 100%;
    animation: ${ballBeat} 0.7s -0.15s infinite linear;
    &:nth-child(2n-1) {
      animation-delay: -0.5s;
    }
  }
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  margin: auto;
  width: auto;
  width: max-content;
  bottom: 0;
  height: max-content;
`
