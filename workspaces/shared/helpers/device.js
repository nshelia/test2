export const device = {
  large: '@media screen and (min-width: 1601px) and (orientation: landscape)',
  mediumPlus: '@media screen and (max-width: 1600px) and (orientation: landscape)',
  medium: '@media screen and (max-width: 1150px) and (orientation: landscape)',
  mobile: '@media screen and (max-width: 768px)',
}
