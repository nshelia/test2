import { createContext } from 'react'

export const DeviceContext = createContext()
export const UserContext = createContext()
