const common = {
  pureWhite: '#FFFFFF',
  crimsonRed: '#BB2026',
  veryBlack: '#282828',
  darkGray: '#3E3E3E',
  mediumGray: '#777777',
  lightGray: '#EEEEEE',
  lightGray2: '#D6D6D6',
  pureBlack: '#222222',
  solidBlack: '#191919',
  almostGray: '#DDDDDD',
  pureGray: '#2E2E2E',
  darkGray2: '#999999',
  pureBlack2: '#262626',

  red: '#DB1D27',
  transparentBlack: 'rgba(17,17,17,0.5)',
  spacing: {
    xs: '2px',
    s: '4px',
    m: '8px',
    l: '16px',
    xl: '32px',
    xxl: '64px',
  },
  typography: {
    xs: '11px',
    s: '13px',
    m: '18px',
  },
}

export const lightTheme = {
  ...common,
  appBgColor: '#F9F9F9',
}

export const darkTheme = {
  ...common,
}
