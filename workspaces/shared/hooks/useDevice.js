import { useEffect, useRef, useState } from 'react'

const mediaQueryList = {
  '(min-width: 1024px) and (orientation: landscape)': 'large',
  '(max-width: 1024px) and (min-width: 768px)': 'tablet',
  '(max-width: 768px)': 'mobile',
}

export const useDevice = () => {
  const queryRefs = useRef([])

  const [currentDevice, setCurrentDevice] = useState('large')

  const listener = (e) => {
    if (e.matches) {
      setCurrentDevice(mediaQueryList[e.media])
    }
  }

  useEffect(() => {
    Object.keys(mediaQueryList).map((item) => {
      const query = window.matchMedia(item)
      query.addListener(listener)
      queryRefs.current[item] = query
      // Get initial device
      if (query.matches) {
        setCurrentDevice(mediaQueryList[query.media])
      }
    })

    return () => {
      Object.keys(mediaQueryList).map((item) => {
        queryRefs.current[item].removeListener(listener)
      })
    }
  }, [])

  return [currentDevice]
}
