import api from 'api'
import { loadState, saveState } from 'helpers/persist'
import { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'

export const useUser = () => {
  const [user, setUser] = useState()
  let history = useHistory()

  useEffect(() => {
    getUser()
  }, [])

  async function fetchUserWithRefreshToken(authData) {
    try {
      const newAuthData = await api.refreshJWTToken(authData.refreshToken)
      saveState(newAuthData, process.env.SESSION_STORAGE_PREFIX)
      const data = await api.getUser()
      setUser(data)
    } catch (e) {
      history.replace('/login')
      saveState(null, process.env.SESSION_STORAGE_PREFIX)
    }
  }

  async function getUser() {
    const authData = loadState(process.env.SESSION_STORAGE_PREFIX)
    if (authData) {
      try {
        const data = await api.getUser()
        setUser(data)
      } catch (e) {
        fetchUserWithRefreshToken(authData)
      }
    } else {
      if (history.location.pathname === '/dashboard') {
        history.replace('/login')
      }
    }
  }

  return [user]
}
