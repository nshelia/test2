import '@babel/polyfill'
import '@root/shared/sass/index.scss'

// import RecoilizeDebugger from 'recoilize'
// window.muxjs = muxjs
import { lightTheme } from '@root/shared/theme'
import { Application } from 'containers/Application'
import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import { RecoilRoot } from 'recoil'
import { ThemeProvider } from 'styled-components'

const root = document.getElementById('appMountPoint')

const App = (
  <ThemeProvider theme={lightTheme}>
    <Router>
      <RecoilRoot>
        <Application />
      </RecoilRoot>
    </Router>
  </ThemeProvider>
)

render(App, root)
