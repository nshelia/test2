import api from '@root/shared/api'
import { useDevice } from '@root/shared/hooks/useDevice'
import { GlobalStyles } from '@root/shared/styled/GlobalStyles'
import { Header } from 'containers/Header'
import React, { useEffect, useRef, useState } from 'react'
import { Route, Switch, useHistory, useParams } from 'react-router-dom'
import { useSetRecoilState } from 'recoil'
import { routes } from 'routes'


function Application() {
  const [device] = useDevice()
  const [authorised, setAuthorised] = useState(false)
  const tries = useRef(1)
  let params = useParams()
  // const setActiveDate = useSetRecoilState(activeDateState)

  // async function pingRequest(td, opts = {}) {
  //   const ERROR_401 = 'ERROR_401'
  //   try {
  //     if (td <= 3) {
  //       await api.syncTime(opts.forceFetchToken)
  //       setAuthorised(true)
  //     } else {
  //       console.log('Maximum tries done')
  //     }
  //   } catch (e) {
  //     if (e === ERROR_401) {
  //       pingRequest(tries.current, { forceFetchToken: true })
  //       tries.current += 1
  //     }
  //   }
  // }

  useEffect(() => {
    if(localStorage.getItem('token') === null){
      getUserToken()
    }
    // async function getUser() {
    //   try {
    //     const response = await api.fetchNewUser()

    //   } catch (e) {
    //     console.log('err')
    //   }
    // }
  }, [])

  async function refreshToken() {
    try {
      const response = await api.getRefreshToken()
      localStorage.setItem('token', response.AccessToken)
      localStorage.setItem('refreshToken', response.RefreshToken)
      console.log(response, 'refresh')
    } catch (e) {
      console.log('err')
    }
  }

  async function getUserToken() {
    try {
      const response = await api.getToken()
      localStorage.setItem('token', response.AccessToken)
      localStorage.setItem('refreshToken', response.RefreshToken)
    } catch (e) {
      console.log('err')
    }
  }


  // if (!authorised) {
  //   return null
  // }

  return (
    <>
      <Switch>
        {routes.map((route) => {
          return (
            <Route
              component={route.component(device)}
              exact={route.exact}
              key={`route-${route.path}`}
              path={route.path}
            />
          )
        })}
      </Switch>
      <GlobalStyles />
    </>
  )
}
export default Application
