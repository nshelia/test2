import { Container as MainContainer } from 'styled-bootstrap-grid'
import styled from 'styled-components'

export const Container = styled(MainContainer)`
  height: 100%;
  justify-content: center;
  display: flex;
`
