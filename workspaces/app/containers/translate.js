import locales from '@root/app/containers/locales'
import React from 'react'


let defaultLocale = 'en'

export const listOfLocales = ['ka', 'en', 'ru']

let currentLocale = null



function localeKey (strings) {
  let key = ''

  strings.forEach((string, index) => {
    key += string

    if (index < strings.length - 1) {
      key += `{${index + 1}}`
    }
  })

  return key
}

export function setLocale (locale) {
  if (!listOfLocales.includes(locale)) {
    throw new TypeError(`Unknown locale! ${locale}`)
  }

  localStorage.setItem('locale', locale)
  defaultLocale = locale
}



export function getLocale (useCurrentLocale = true) {
  if (!useCurrentLocale) {
    const locale = localStorage.getItem('locale')

    return (
      listOfLocales.includes(locale)
        ? locale
        : null
    )
  }

  if (currentLocale) {
    return currentLocale
  }

  const locale = localStorage.getItem('locale')

  currentLocale = listOfLocales.includes(locale)
    ? locale
    : defaultLocale

  return currentLocale
}

export function t (strings, ...values) {
  const locale = getLocale()
  const localeStrings = locales.get(locale)
  const key = localeKey(strings)
  let localeString = key

  if (typeof localeStrings !== 'undefined') {
    if (typeof localeStrings.get(key) !== 'undefined') {
      localeString = localeStrings.get(key)
    }
  }

  if (process.env.NODE_ENV === 'development') {
    reportLocale(key)
  }

  values.forEach((value, index) => {
    localeString = localeString.replace(`{${index + 1}}`, value)
  })

  return localeString
}
