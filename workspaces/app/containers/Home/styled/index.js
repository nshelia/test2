import styled, {css} from 'styled-components';
import { Link } from 'react-router-dom'

export const StyledHome = styled.div`
  position: relative;
  font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
`
export const StyledLandingWrapper = styled.div`
  font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
height: 100%;
display: flex;
flex-direction: column;
  
`
export const StyledLangList = styled.div`
position: absolute;
display: flex;
align-items: center;
width: 113px;
height: 41px;
background: #FFFFFF;
border-radius: 4px;
padding: 8px;
justify-content: space-between;
div{
  display: flex;
    align-items: center;
    justify-content: space-between;
    font-size: 16px;
line-height: 21px;
height: 41px;
letter-spacing: -0.32px;

color: #464646;
    svg{
      margin-top: 4px;
      margin-right: 10px;
    }
}
`
export const Styledgradient = styled.div`
    background: linear-gradient(rgb(69, 114, 233), rgb(117, 201, 250));
    height: 614px;
    width: 100%;
    position: absolute;
    z-index: 3;
    opacity: 0.92;
`
export const StyledBackground = styled.img`
  width: 100%;
  height: 614px;
`
export const StyledLadnigHeader = styled.div`
width: 100%;
height: 614px;
`

export const StyledLogoWrapper = styled.div`

`
export const StyledHeaderTabs = styled.div`
  display:  flex;
  justify-content: space-between;
  width: 100%;
`

export const StyledHeaderContent = styled.div`
  position: absolute;
  z-index: 4;
  top: 0;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`
export const StyledFirst = styled.div`
  display: flex;
  padding-left: 98px;
  padding-top: 32px;
`

export const StyledTabs = styled.div`
  display: flex;
  gap: 0 42px;
  margin-top: -15px;
  margin-left: 30px;
  color: #ffffff;
  font-size: 20px;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofia'};

  div{
    align-items: center;
    display: flex;
    cursor: pointer;
    &:hover{
      font-weight: ${props => props.geo ? 'bold' : 'normal'};
      opacity: 0.8;
    }
  }
`
export const StyledSecond = styled.div`
  display: flex;
  align-items: center;
  margin-right: 100px;
  padding-top: 16px;
`
export const StyledBuisness =  styled(Link)`
  font-weight: bold;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  color: #ffffff;
  font-size: 20px;
  align-items: center;
    display: flex;
    cursor: pointer;
    &:hover{
      text-decoration: underline;
    }
`
export const StyledDiv = styled.div`

`
export const StyledStart4 = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 232px;
height: 50px;
background: #2E5AD3;
border-radius: 8px;
color: #ffffff;
  border-radius: 10px;
  font-size: 18px;
  font-family: 'robotoNusx';
  cursor: pointer;
  &:hover{
    opacity: 0.8;
  }
  span{
    svg{
      margin-bottom: 10px;
    }
    margin: 5px;
   font-size: 34px;
  }
`
export const StyledStart = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: #004AA3;
  width: 210px;
  height: 50px;
  background: #ffffff;
  border-radius: 10px;
  font-size: 18px;
  font-family: ${props => props.geo ? 'robotNusx' : 'sofia'};
  cursor: pointer;
  &:hover{
    opacity: 0.8;
  }
  span{
    margin: 5px;
    margin-top: 10px;
    font-size: 20px;
    font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  }
`
export const StyledStarts = styled(Link)`
margin-top: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #004AA3;
  width: 232px;
  height: 60px;
  background: #ffffff;
  border-radius: 10px;
  font-size: 20px;
  cursor: pointer;
  font-family: ${props => props.geo ? 'robotNusx' : 'sofia'};
  &:hover{
    opacity: 0.8;
  }
  span{
    font-size: 24px;
    font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  }
`
export const StyledLanguage = styled.div`
  margin-left: 17px;
  cursor: pointer;
  position: relative;
  ${StyledLangList}{
    opacity: 0;
    transition: opacity 0.3s;
  }
  &:hover{
    ${StyledLangList}{
      opacity: 1;
  }
  }
`
export const StyledText = styled.div`
width: 1024px;
 font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
font-size: 50px;
line-height: 72.5px;
color: #ffffff;
margin-top: 53px;
display: flex;
align-items: center;
text-align: center;
justify-content: center;
`
export const StyledsubText = styled.div`
color: #EDEDED;
    font-size: 27px;
    text-align: center;
    font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
    width: 800px;
    margin-top: 45px;
    line-height: 40px;
`


export const StyledRow = styled.div`
font-family: 'sofia';
display: flex;
justify-content: center;
margin-top: 40px;
`
export const StyledRowText = styled.div`
display: flex;
flex-direction: column;
width: 50%;
justify-content: center;
padding-left: 100px;
`
export const StyledRowText1 = styled.div`
display: flex;
flex-direction: column;
width: 50%;
justify-content: center;
padding-left: 300px;
`
export const StyledImage = styled.div`
  width: 593px;
  height: 475px;
`
export const Styledtextes = styled.div`
  color: #444444;
  font-size: 20px;
  width: 600px;
  font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
  line-height: 24px;
  margin-top: 15px ;
  display: flex;
  flex-direction: column;
  span{
    margin-top: 8px;
  }
  svg{
    margin-right: 5px;
  }
`
export const StyledHead = styled.div`
color:#111111;
  font-size: 36px;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
`
export const StyledStart1 = styled.div`
margin-top: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #ffffff;
  width: 220px;
  height: 50px;
  background: #2E5AD3;
  border-radius: 10px;
  font-size: 18px; 
  cursor: pointer;
  font-family: ${props => props.geo ? 'robotNusx' : 'sofia'};
  &:hover{
    opacity: 0.8;
  }
  span{
    font-size: 20px;
    margin: 5px;
    margin-top: 10px;
    font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  }
`
export const StyledLogos = styled.div`
  display:  flex;
  font-size: 16px;
  color: #666666;
  margin-top: 18px;
  align-items: center;
  font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
  border-right:  ${props => props.chrome ? '1px solid #BFBFBF' : ''};
  padding-right:  ${props => props.chrome ? '15px' : '0'};
  cursor: pointer;
  svg{
    margin-right: 5px;
  }
  &:hover{
    opacity: 0.8;
  }
`
export const StyledChrome = styled.div`
    background: linear-gradient(#F6FBFF, #DDEFFF);
    height: 443px;
    display:  flex;
    flex-direction: column;
    align-items: center;
    margin-top: 40px;
`
export const StyledChromeText = styled.div`
  color: #444444;
  font-size: 24px;
  display: flex;
  justify-content: center;
  text-align: center;
  font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
  margin-top: 31px;
  width: 900px;
`
export const StyledChromeTitle = styled.div`
  color: #111111;
  font-size: 36px;
  display: flex;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  justify-content: center;
    margin-top: 58px;
  `
export const StyledChromeIcons = styled.div`
display: flex;
padding: 20px 0;
div{
  border-right: 1px solid #BFBFBF;
}
svg{
  margin: 20px 50px;
}
`
export const StyledBusiness = styled.div`
  display: flex;
  background: linear-gradient(#F6FBFF, #DDEFFF);
  height: 443px;
  padding: 0 100px ;
`
export const StyledBusText = styled.div`
 display:  flex;
 flex-direction: column;
 margin-top: 78px;
 width: 50%;
`
export const StyledHead1 = styled.div`
  color:#111111;
  font-size: 36px;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  width: 660px;
`
export const StyledIconsWrapper = styled.div`
position: relative;
`

export const StyledStart2 = styled.div`
margin-top: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #ffffff;
  width: 278px;
  height: 60px;
  background: #2E5AD3;
  border-radius: 10px;
  font-size: 20px;
  cursor: pointer;
  &:hover{
    opacity: 0.8;
  }
  span{
    margin: 5px;
    margin-top: 10px;
    font-size: 24px;
    font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  }
`
export const StyledIcon1 = styled.div`
position: absolute;
top: 52px;
left: 100px;
`
export const StyledIcon2 = styled.div`
 position: absolute;
    top: 217px;
    left: 334px;
`
export const StyledFooter = styled.div`
  display: flex;
  justify-content: space-between;
  height: 400px;
  padding: 100px ;
  padding-right: 30%;
  img{
    width: 100px;
    height: 84px;
  }
`
export const StyledList = styled.div`
 display: flex;
 flex-direction: column;
 span{
  width: 200px;
  font-size: 20px;
  color: #444444;
  margin-bottom:16px ;
 }
`
export const StyledListTitle = styled.div`
  font-size: 24px;
  color: #111111;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  margin-bottom: 24px;
`

export const StyledBottom = styled.div`
  display:  flex;
  background:#F6FBFF ;
  color: #444444;
  font-size: 16px;
  width: 100%;
  height: 80px;
  align-items: center;
  justify-content: center;
`
export const StyledSpan = styled.span`

display: flex;
padding-left: 10px;
font-weight: 300;
font-size: 16px;
line-height: 23px;
align-items: center;
color: #111111;
margin-top: 14px;
svg{
  min-width: 12px;
  margin-right: 16px;
}
`
export const StyledPriceElements = styled.div`
display: flex;
margin-top: 4px;
flex-direction: column;

`
export const StyledPriceList = styled.div`
display: flex;
flex-direction: column;
width: 450px;
padding: 0 60px;
border-left: ${props => props.border ? '1px solid #BFBFBF;' : 'none'} ;
${StyledStart}{
  margin-bottom: 10px;
}
${StyledStart4}{
  margin-bottom: 10px;
}
`
export const StyledPrices = styled.div`
display: flex;
flex-direction: column;
align-items: center;
padding: 100px 0px;
`
export const StyledPriceTitle = styled.div`
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
font-style: normal;
font-weight: 500;
font-size: 36px;
line-height: 51px;
color: #111111;
`
export const StyledpriceText = styled.div`
  font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
font-size: 24px;
line-height: 106.6%;
text-align: center;
margin-top: 21px;
color: #444444;
`
export const StyledPriceWrapper = styled.div`
  display: flex;
  margin-top: 60px;
  ${StyledStart}{
    width: 232px;
    height: 50px;

    border: 1px solid #2E5AD3;
    border-radius: 8px;
  }
`
export const StyledPriceText = styled.div`
 font-weight: 300;
font-size: 24px;
line-height: 106.6%;
text-align: center;
color: #444444;
font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
margin-top: 20px;
`
export const StyledPlistText = styled.div`
margin-top: 16px;
font-weight: 300;
font-size: 16px;
line-height: 106.6%;
color: #444444;
height: 42px;
padding-left: 10px;
`

export const StyledPlistTitle = styled.div`
font-weight: 500;
font-size: 24px;
line-height: 106.6%;
color: #111111;
font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
padding-left: 10px;
`
export const StyledLogoDiv = styled.div`
  display: flex;
  margin-top: 15px;
  gap: 0 15px;
  align-items:center ;
  svg{
    width: 30px;
    height: 30px;
  }
`

export const StyledIconsFooter = styled.div`
display:flex;
gap: 10px ;
`