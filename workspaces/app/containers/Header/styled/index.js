import { device } from '@root/shared/helpers/device'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const StyledHeader = styled.div`
  position: fixed;
  z-index: 92;
  left: 0;
  top: 0;
  background: ${(props) => props.theme.pureBlack};
  width: 60px;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  ${device.medium} {
    width: 48px;
  }
`
export const StyledLogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 18px;
  align-items: center;
  width: 100%;
  height: 26px;
  cursor: pointer;
  ${device.medium} {
    svg {
      width: 20px;
      height: 22px;
    }
  }
`

export const StyledMenuIcon = styled(Link)`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${(props) => props.theme.pureBlack};
  transition: background-color 0.2s ease;
  cursor: pointer;
  svg {
    path {
      fill: ${(props) => (props.active ? props.theme.crimsonRed : props.theme.almostGray)};
    }
  }
  &:hover {
    background: ${(props) => props.theme.crimsonRed};
    svg {
      path {
        fill: ${(props) => props.theme.pureWhite};
      }
    }
  }
  ${device.medium} {
    svg {
      width: 13px;
    }
  }
`
export const StyledMenuIconsWrapper = styled.div`
  margin-top: 70px;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`

export const StyledHeaderSectionWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`
