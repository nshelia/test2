import styled, {css} from 'styled-components';


export const StyledPageWrapper = styled.div`
font-family: 'inter';
height: 100%;
  
`
export const StyledHeader = styled.header`
height: 80px;
background: #ffffff;
display: flex;
justify-content: flex-end;
align-items: center;
border-bottom: 1px solid #E3E3E3;
`
export const StyledHeaderElements = styled.div`
  display: flex;
`
export const StyledChromeIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 8px 20px;
  color: #4b4b4b;
  font-weight: 500;
  font-size: 14px;
  gap: 10px;
  background: #FFFFFF;
  border: 1px solid #DFDFDF;
  border-radius: 8px;
  cursor: pointer;
  &:hover{
    opacity: 0.8;
  }
`

export const StyledUpgrade = styled.div`
  background: #2D62ED;
  padding: 4px 20px;
  cursor: pointer;
  color: #FFFFFF;
  font-weight: 600;
  font-size: 14px;
  line-height: 28px;
  border-radius: 6px;
  margin-left: 16px;
  &:hover{
    opacity: 0.8;
  }

 `

 export const StyledAvatarWrapper = styled.div`
  height: 36px;
  margin-left: 80px;
  margin-right: 35px;
  display: flex;
  align-items: center;
  svg{
    cursor: pointer;
    &:hover{
      opacity:  0.6;
    }
  }
 `

 export const StyledImg = styled.img`
  width: 36px;
  height: 36px;
 `

export const StyledMainWrapper = styled.div`
  height: 100%;
  display: flex;
`
export const StyledLeftBar = styled.div`
  background: #2D62ED;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.05);
  width: 177px;
  height: calc(100% - 115px);
  display: flex;
  flex-direction: column;
  justify-content:  space-between;
  padding-top: 115px;
`
export const StyledItemsTop = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`
export const StyledItemsBottom = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 0;
  align-items: flex-end;
`
export const StyledActiveAngle = styled.div`
  position: absolute;
  right: 0;
  display: none;
  ${props => props.top && css`
    top: -14px;
  `}
  ${props => props.bottom && css`
    bottom: -14px;
  `}
`
export const StyledNavBarLabel = styled.div`
  margin-left: 12px;
  color: #FFFFFF;
  font-weight: 400;
  font-size: 12px;
`
export const StyledBorderWrapper = styled.div`
  margin: 10px 0;
  width: 100%;
  display: flex;
  justify-content: center;
`
export const StyledBorder = styled.div`
  width: calc(100% - 26px);
  height: 2px;
  background: #9EB9FF;
`
export const StyledNavBarItems = styled.div`
  cursor: pointer;
  width: 150px;
  height: 38px;
  display: flex;
  padding-left: 16px;
  background: transparent;
  align-items: center;
  position: relative;
  svg{
      path{
        fill: #FFFFFF;
      }
    }
  ${props => props.active && css`
    background: #F8F8F8;
    border-radius: 30px 0px 0px 30px;
    ${StyledNavBarLabel}{
      color: #2D62ED;
    }
    svg{
      path{
        fill: #2D62ED;
      }
    }
    ${StyledActiveAngle}{
      display: block;
        svg{
          path {
          fill: #ffffff;
        }
      }
    }
  `}
  ${props => props.disabled && css`
pointer-events: none;
    ${StyledNavBarLabel}{
      opacity: 0.5;
    }
    svg{
      opacity: 0.5
    }
  `}
  &:hover{
    border-radius: 30px 0px 0px 30px;
    background: #F8F8F8;
    ${StyledNavBarLabel}{
      color: #2D62ED;
    }
    svg{
      path{
        fill: #2D62ED;
      }
    }
    ${StyledActiveAngle}{
      display: block;
        svg{
          path {
          fill: #ffffff;
        }
      }
    }
  }
`
export const StyledSwitcher = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 136px;
  width: 100%;
`


export const StyledMainContent = styled.div`
  flex-direction: column;
  width: calc(100% - 504px);
  height: 100vh;
  background:#F8F8F8;
  padding: 0 92px;
`

export const StyledSwWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center ;
  gap: 12px;
  border:  0.7px solid #DBE9FD;
  padding: 4px;
  border-radius: 40px;
  svg{
    cursor: pointer;
    path{
      fill: #ffffff;
    }
    &:hover{
      path{
        fill: #E6D131;
      }
    }
  }
`
export const StyledInputHeader = styled.div`
  display:  flex;
  width: 100%;
  height: 80px;
  border-bottom: 1px solid #EDEDED;
  align-items: flex-end;
  justify-content: space-between;
`
export const StyledPremium = styled.div`
    width: 100%;
    display:flex;
    justify-content:flex-end;
    margin-top:40px ;
    div{
      width: 206px;
height: 43px;
background: #2D62ED;
border-radius: 39px;
font-weight: 700;
font-size: 14px;
line-height: 20px;
color: #FFFFFF;
display: flex;
align-items: center;
justify-content:center ;
gap: 0 12px;
cursor: pointer;
    }
`
export const StyledCheckButton = styled.div`
  padding: 8px 20px;
  gap: 4px;
  background: #2D62ED;
  border-radius: 6px;
  display: flex;
  align-items: center;
  cursor: pointer;
  font-weight: 600;
  font-size: 14px;
  color: #ffffff;
  &:hover{
    opacity: 0.8;
  }
`
export const StyledbuttonsWrapper = styled.div`
  display: flex;
  margin-bottom: 14px;
`
export const StyledInputButtons = styled.div`
  padding: 8px 14px;
  gap: 8px;
  background: #F8F8F8;
  border-radius: 4px;
  margin: 0 15px;
  display: flex;
  align-items: center;
  justify-content: end;
  cursor: pointer;
  background: transparent;
  svg{
    path{
      fill:#606060;
    }
  }
  color: #606060;
  ${props => props.active && css`
  svg{
    path{
      fill: #2D62ED;
    }
  }
  color: #2D62ED;
  background: #EDEDED;
  `}

  &:hover{
    svg{
    path{
      fill: #2D62ED;
    }
  }
  color: #2D62ED;
  background: #EDEDED;
  }
`
export const StyledDropDown = styled.div`
      position: absolute;
    z-index: 20;
    height: 200px;
    overflow: auto;
    margin-top: 37px;
    background: #ffffff;
    gap: 8px 0;
    visibility: hidden;

`
export const StyledLanguage = styled.div`
  position: relative;
  width: 144px;
  background: #F8F8F8;
  border-radius: 4px;
  height: 36px;
  display: flex;
  justify-content: center;
  gap: 8px;
  padding: 8px 14px;
  margin-right: 20px;
  cursor: pointer;
  svg{
    margin-top: -3px;
  }
  &:hover{
  div{
    visibility: visible !important;

  }
}
`
export const StyledLineBetween = styled.div`
  height: 24px;
  width: 1px;
  background: #CCCACA;
  margin-top: 8px;
  margin-right: 16px;
`
export const StyledInputWrapper = styled.div`
position: relative;
  /* height: calc(100% - 247px);
  padding-top: 52px;
  overflow-y: auto; */
`

export const StyledInputElement = styled.div`
  height: 100%;
  position: relative;
`
export const StyledInput = styled.textarea`
  width: 100%;
  height: 99%;
  border: none;
  outline: none;
  padding: 0;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  color: #6F6F6F;
  background: transparent;
  font-family: sans-serif;
  letter-spacing: 0.3px;
  position: absolute;
  z-index: 2;

`

export const StyledInputFooter = styled.div`
  width: 100%;
  height: 118px;
  border-top: 1px solid #EDEDED;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`
export const StyledLogo = styled.div`
  margin-right: 130px;
`
export const StyledFooterIcons = styled.div`
  display: flex;
  gap: 30px;
  margin-left: 130px;
  svg{
    cursor: pointer;
    &:hover{
      opacity: 0.8;
    }
  }
`
export const StyledWords= styled.div`
width: 192px;
height: 36px;
padding: 4px 12px ;
background: #ffffff;
font-size: 12px;
line-height: 20px;
text-align: center;
color: #000000;
display:  flex;
align-items: center;
`
export const StyledRedLineWrapper = styled.div`
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  font-family: sans-serif;
  position: absolute;
  top: 0px;
  letter-spacing: 0.3px;
  z-index: 1;
`
export const StyledRighBar = styled.div`
  min-width: 300px;
  width: 360px;
  height: 100vh;
  background: #F8F8F8;
`
export const StyledRighBarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
`
export const StyledRightHead = styled.div`
width: 300px;
height: 52px;
background: #2D62ED;
opacity: 0.9;
border-radius: 50px;
margin-top: 40px;
display: flex;
align-items: center;
justify-content: center;
gap: 45px;
font-size: 12px;
line-height: 20px;
div{
  cursor: pointer;
  color: #ECECEC;
}

`
export const StyledConnect = styled.div`
width: 157px;
height: 36px;
background: #FFFFFF;
border-radius: 40.0601px;
display: flex ;
padding: 0 20px;
align-items: center;
justify-content: space-between;
div{
font-size: 16px;
line-height: 20px;
font-family: 'sofiaBold';
color: #000000;
}
span{
font-size: 12px;
line-height: 20px;
color: #6F6F6F;
}
`
export const StyledDict = styled.div`
width: 299px;
min-height: 188px;
padding: 21px 16px;
display: flex;
flex-direction: column;
justify-content: space-between;

background: #FFFFFF;
box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
border-radius: 10px;
`
export const StyledNEwListWrapper = styled.div`
position: relative;
margin-top: 28px;
display: flex;
justify-content: center;
`
export const StyledRightWrapper = styled.div`
margin-top: 28px;
  position: relative;
  height: 70vh;
  width: 100%;
  overflow-y: auto;
`
export const StyledSpeling = styled.div`
  display: flex;
  gap: 10px;
  flex-direction: row;
  align-items: center;
  font-size: 10px;
line-height: 20px;
/* identical to box height, or 200% */

font-family: 'sofiaBold';
font-weight: bold;
color: #7D7D7D;
`
export const StyledMistakes = styled.div`
  display: flex;
  ul{
    margin: 0;
    padding-inline-start: 20px;
  }
  li{
    margin-top: 5px;
    padding: 2px 6px;
gap: 10px;
height: 24px;
font-size: 12px;
line-height: 20px;
color: #FFFFFF;
background: #2D62ED;
border-radius: 5px;
cursor: pointer;
  }
`
export const StyledNewMist = styled.div`
display: flex;
align-items: center;
justify-content: space-between;
width: 300px;
height: 48px;
cursor: pointer;

background: #FFFFFF;
box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
border-radius: 10px;
font-size: 12px;
line-height: 20px;
color: #393939;
font-weight: bold;
svg{
  margin: 0 20px;
}
`