import styled, {css} from 'styled-components';
import { Link } from 'react-router-dom'

export const StyledHome = styled.div`
  position: relative;
  font-family: 'sofia';
`
export const StyledLandingWrapper = styled.div`
font-family: 'sofia';
height: 100%;
display: flex;
flex-direction: column;
  
`
export const Styledgradient = styled.div`
    background: rgba(46, 90, 211, 0.95);
    height: 614px;
    width: 100%;
    position: absolute;
    z-index: 3;
`
export const StyledBackground = styled.img`
  width: 100%;
  height: 614px;
`
export const StyledLadnigHeader = styled.div`
width: 100%;
height: 614px;
`

export const StyledLogoWrapper = styled.div`

`
export const StyledHeaderTabs = styled.div`
  display:  flex;
  justify-content: space-between;
  width: 100%;
`

export const StyledHeaderContent = styled.div`
  position: absolute;
  z-index: 4;
  top: 0;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`
export const StyledFirst = styled.div`
  display: flex;
  padding-left: 98px;
  padding-top: 32px;
`

export const StyledTabs = styled.div`
   display: flex;
  gap: 0 42px;
  margin-top: -15px;
  margin-left: 30px;
  color: #ffffff;
  font-size: 18px;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofia'};

  div{
    align-items: center;
    display: flex;
    cursor: pointer;
    &:hover{
      font-weight: ${props => props.geo ? 'bold' : 'normal'};
      opacity: 0.8;
    }
  }
`
export const StyledSecond = styled.div`
  display: flex;
  align-items: center;
  margin-right: 100px;
  padding-top: 32px;
`
export const StyledBuisness =  styled(Link)`
  font-weight: bold;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  color: #ffffff;
  font-size: 20px;
  align-items: center;
    display: flex;
    cursor: pointer;
    text-decoration:underline ;
`
export const StyledDiv = styled.div`

`

export const StyledStart = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: #004AA3;
  width: 210px;
  height: 50px;
  background: #ffffff;
  border-radius: 10px;
  font-size: 18px;
  cursor: pointer;
  &:hover{
    opacity: 0.8;
  }
  span{
    font-family: 'sofiaBold';
  }
`
export const StyledStarts = styled.div`
margin-top: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #004AA3;
  width: 232px;
  height: 60px;
  background: #ffffff;
  border-radius: 10px;
  font-size: 24px;
  cursor: pointer;
  &:hover{
    opacity: 0.8;
  }
  span{
    font-family: 'sofiaBold';
  }
`
export const StyledLangList = styled.div`
position: absolute;
display: flex;
align-items: center;
width: 113px;
height: 41px;
background: #FFFFFF;
border-radius: 4px;
padding: 8px;
justify-content: space-between;
div{
  display: flex;
    align-items: center;
    justify-content: space-between;
    font-size: 16px;
line-height: 21px;
height: 41px;
letter-spacing: -0.32px;

color: #464646;
    svg{
      margin-top: 4px;
      margin-right: 10px;
    }
}
`
export const StyledLanguage = styled.div`
  margin-left: 34px;
  cursor: pointer;
  position: relative;
  ${StyledLangList}{
    opacity: 0;
    transition: opacity 0.3s;
  }
  &:hover{
    ${StyledLangList}{
      opacity: 1;
  }
  }
`
export const StyledText = styled.div`
width: 850px;
 font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
font-size: 45px;
line-height: 52px;
color: #ffffff;
margin-top: 53px;
display: flex;
align-items: center;
justify-content: center;
`
export const StyledsubText = styled.div`
color: #EDEDED;
font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
font-size: 25px;
    width: 1000px;
    margin-top: 36px;
    line-height: 40px;
    display: flex;
    flex-direction: column;
    a{
      color: #ffffff;
      font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
      text-decoration: underline;
      width: 186px;
      height: 48px;
      margin-top: 13px;
      display: flex;
      align-items: center;
      justify-content: center;
      font-size: 18px;
      line-height: 24px;
      background: #FFFFFF;
      border: 1px solid #2E5AD3;
      border-radius: 7px;
      cursor: pointer;
      color: #2E5AD3;
    }
`

export const StyledRow = styled.div`
font-family: 'sofia';
display: flex;
justify-content: center;
margin-top: 40px;
`
export const StyledRowText = styled.div`
display: flex;
flex-direction: column;
width: 50%;
justify-content: center;
padding-left: 100px;
`
export const StyledRowText1 = styled.div`
display: flex;
flex-direction: column;
width: 50%;
justify-content: center;
padding-left: 350px;
`
export const StyledImage = styled.div`
  width: 593px;
  height: 475px;
`
export const Styledtextes = styled.div`
  color: #444444;
  font-size: 20px;
  width: 620px;
  margin-top: 13px ;
  line-height: 26px;
  svg{
    margin-right: 5px;
    margin-top: -6px;
  }
`
export const StyledHead = styled.div`
color:#111111;
  font-size: 36px;
  font-family: 'sofiaBold';
`
export const StyledStart1 = styled.div`
margin-top: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #ffffff;
  width: 220px;
  height: 50px;
  background: #2E5AD3;
  border-radius: 10px;
  font-size: 18px;
  cursor: pointer;
  &:hover{
    opacity: 0.8;
  }
  span{
    font-family: 'sofiaBold';
  }
`
export const StyledLogos = styled.div`
  display:  flex;
  font-size: 16px;
  color: #666666;
  margin-top: 18px;
  svg{
    margin-right: 5px;
  }
`
export const StyledChrome = styled.div`
    background: linear-gradient(#F6FBFF, #DDEFFF);
    height: 443px;
    display:  flex;
    flex-direction: column;
    align-items: center;
`
export const StyledChromeText = styled.div`
  color: #444444;
  font-size: 24px;
  display: flex;
  justify-content: center;
  text-align: center;
  margin-top: 31px;
  width: 900px;
`
export const StyledChromeTitle = styled.div`
  color: #111111;
  font-size: 36px;
  display: flex;
  font-family: 'sofiaBold';
  justify-content: center;
    margin-top: 58px;
  `
export const StyledChromeIcons = styled.div`
svg{
  margin: 40px 50px;
}
`
export const StyledBusiness = styled.div`
  display: flex;
  background: linear-gradient(#F6FBFF, #DDEFFF);
  height: 443px;
  padding: 0 100px ;
`
export const StyledBusText = styled.div`
 display:  flex;
 flex-direction: column;
 margin-top: 78px;
 width: 50%;
`
export const StyledHead1 = styled.div`
  color:#111111;
  font-size: 36px;
  font-family: 'sofiaBold';
  width: 400px;
`
export const StyledIconsWrapper = styled.div`
position: relative;
`

export const StyledStart2 = styled.div`
margin-top: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #ffffff;
  width: 278px;
  height: 60px;
  background: #2E5AD3;
  border-radius: 10px;
  font-size: 24px;
  cursor: pointer;
  &:hover{
    opacity: 0.8;
  }
  span{
    font-family: 'sofiaBold';
  }
`
export const StyledIcon1 = styled.div`
position: absolute;
top: 52px;
left: 100px;
`
export const StyledIcon2 = styled.div`
 position: absolute;
    top: 217px;
    left: 334px;
`
export const StyledFooter = styled.div`
  display: flex;
  justify-content: space-between;
  height: 400px;
  padding: 100px ;
  padding-right: 30%;
  img{
    width: 100px;
    height: 84px;
  }
`
export const StyledList = styled.div`
 display: flex;
 flex-direction: column;
 span{
  width: 200px;
  font-size: 20px;
  color: #444444;
  margin-bottom:16px ;
 }
`
export const StyledListTitle = styled.div`
  font-size: 24px;
  color: #111111;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  margin-bottom: 24px;
`

export const StyledBottom = styled.div`
  display:  flex;
  background:#F6FBFF ;
  color: #444444;
  font-size: 16px;
  width: 100%;
  height: 80px;
  align-items: center;
  justify-content: center;
`
export const StyledHeaderImage = styled.div`
width: 50%;
display: flex;
justify-content:flex-end ;
`
export const StyledHeaderInfo = styled.div`
display: flex;
align-items: center;
width: 100%;
display: flex;
padding: 0 100px;
`
export const StyledInfo = styled.div`
  display: flex;
  flex-direction: column;
  width: 631px;
  width: 50%;
`
export const StyledMainInfo = styled.div`
display: flex;
flex-direction:  column;
font-family:'sofia';
`
export const StyledBusRow = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 40px;
`
export const StyledTitleInfo = styled.div`
  color: #111111;
  margin: 80px 0;
  font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
  font-size: 40px;
  display: flex ;
  justify-content: center;
`

export const StyledProduct= styled.div`
width: 450px;
background: #FFFFFF;
border: 1px solid #E6E6E6;
box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.12);
border-radius: 8px;
padding: 40px 15px;
display: flex;
    flex-direction: column;
    align-items: center;
    margin: 0 25px;
`
export const StyledProdTitle = styled.div`
font-size: 22px;
line-height: 25px;
display: flex;
align-items: center;
text-align: center;
justify-content: center;
font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
color: #333333;
`
export const StyledProdText = styled.div`
  font-size: 14px;
line-height: 20px;
height: 200px;
color: #222222;
text-align: center;
display:flex ;
flex-direction:column ;
/* text-align: ${props => props.center ? 'center' : 'left'};
span{
  padding-left: 60px;
} */
`

export const StyledSpan1 = styled.div`
  /* text-align:center;
  padding-left: 0 !!important; */
   `
export const StyledProdImg = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 44px 0;
  svg{
    width: 90px;
    height: 90px;
  }
`
export const StyledProdButt1 = styled.a`
width: 186px;
height: 48px;

background: #2E5AD3;
border: 1px solid rgba(17, 18, 18, 0.05);
border-radius: 7px;
margin-top: 40px;
font-weight: 400;
font-size: 16px;
line-height: 24px;
display: flex;
align-items: center;
justify-content: center;
color: #ffffff;
cursor: pointer;
&:hover{
  opacity: 0.8;
}
`
export const StyledProdButt2 = styled.a`
width: 186px;
height: 48px;
margin-top: 13px;
display: flex;
align-items: center;
justify-content: center;
font-size: 16px;
line-height: 24px;
background: #FFFFFF;
border: 1px solid #2E5AD3;
border-radius: 7px;
cursor: pointer;
color: #2E5AD3;
&:hover{
  opacity: 0.8;
}
`
export const StyledNewButton = styled.a`
display: flex;
justify-content: center;
align-items: center;
padding: 8px 30px 12px;
width: 278px;
height: 60px;
margin-top: 45px;
background: #2E5AD3;
font-size: 24px;
line-height: 100%;
letter-spacing: -0.408px;
color: #FFFFFF;
border-radius: 8px;
cursor: pointer;
`
export const StyledNewText = styled.div`
    width: 1100px;
    font-style: normal;
    font-weight: 300;
    font-size: 24px;
    line-height: 29px;
    text-align: center;
    margin-top: 15px;
    color: #444444;
    font-family: ${props => props.geo ? 'robotoNusx' : 'sofia'};
    display: flex;
    flex-direction: column;
    b{
      font-weight: bold;
    }
`
export const StyledNewTitle = styled.div`
font-size: 40px;
line-height: 57px;
color: #111111;
font-family: ${props => props.geo ? 'roboto-mt' : 'sofiaBold'};
`
export const StyledNewInfo = styled.div`
height: 500px;
background: #F7FBFF;
display:  flex;
flex-direction:column ;
align-items: center;
padding-top: 58px;
margin-top: 100px;
`
export const StyledIconsFooter = styled.div`
  display: flex;
  gap: 10px;
`