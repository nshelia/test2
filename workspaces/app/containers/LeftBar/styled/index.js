import styled, {css} from 'styled-components';


export const StyledLeftBar = styled.div`
  font-family: 'sofia';
  background: #2D62ED;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.05);
  width: 194px;
  min-width: 190px;
  height: auto;
  min-height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`
export const StyledUserLogin = styled.div`
margin: 52px 0;
width: 170px;
  display: flex;
  font-size: 14px;
line-height: 17px;
color: #FFFFFF;
align-items: center;
span{
  margin-left: 12px;
}
`
export const StyledLogos = styled.div`
  display:  flex;
  font-weight: 400;
font-size: 14px;
line-height: 17px;
padding-left: 25px;
cursor: pointer;
color: #FFFFFF;
  margin-top: 18px;
  svg{
    margin-right: 5px;
  }
`
export const StyledImg = styled.img``
export const StyledItemsTop = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`
export const StyledItemsBottom = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 0;
`
export const StyledActiveAngle = styled.div`
  position: absolute;
  right: 0;
  display: none;
  ${props => props.top && css`
    top: -14px;
  `}
  ${props => props.bottom && css`
    bottom: -14px;
  `}
`
export const StyledNavBarLabel = styled.div`
  margin-left: 12px;
  color: #FFFFFF;
  font-weight: 400;
  font-size: 12px;
`
export const StyledBorderWrapper = styled.div`
  margin: 40px 0;
  width: 100%;
  display: flex;
  justify-content: center;
`
export const StyledBorder = styled.div`
  width: calc(100% - 26px);
  height: 2px;
  background: #9EB9FF;
`
export const StyledUpgrade = styled.div`
cursor: pointer;
font-weight: 700;
font-size: 12px;
line-height: 16px;
color: #2D62ED;
display: flex;
width: 145px;
height: 47px;
padding: 5px 20px;
margin-left: 20px;
gap: 0 8px;
background: #FFFFFF;
border-radius: 8px;
display: flex;
align-items: center;
img{
  height: 24px;
}
`
export const StyledNavBarItems = styled.div`
margin-left: 9px;
  cursor: pointer;
  width: 181px;
  height: 38px;
  display: flex;
  padding-left: 16px;
  background: transparent;
  align-items: center;
  position: relative;
  svg{
      path{
        fill: #FFFFFF;
      }
    }
  ${props => props.active && css`
    background: #F8F8F8;
    border-radius: 30px 0px 0px 30px;
    ${StyledNavBarLabel}{
      color: #2D62ED;
    }
    svg{
      path{
        fill: #2D62ED;
      }
    }
    ${StyledActiveAngle}{
      display: block;
        svg{
          path {
          fill: #ffffff;
        }
      }
    }
  `}
  ${props => props.disabled && css`
pointer-events: none;
    ${StyledNavBarLabel}{
      opacity: 0.5;
    }
    svg{
      opacity: 0.5
    }
  `}
  &:hover{
    border-radius: 30px 0px 0px 30px;
    background: #F8F8F8;
    ${StyledNavBarLabel}{
      color: #2D62ED;
    }
    svg{
      path{
        fill: #2D62ED;
      }
    }
    ${StyledActiveAngle}{
      display: block;
        svg{
          path {
          fill: #ffffff;
        }
      }
    }
  }
`
export const StyledSwitcher = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 50px;
  width: 100%;
`
export const StyledSwWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center ;
  gap: 12px;
  padding: 4px;
  div{
    cursor: pointer;
  }
  svg{
    cursor: pointer;
    path{
      fill: #ffffff;
    }
    &:hover{
      path{
        fill: #E6D131;
      }
    }
  }
  `
  export const StyledLogoWrapper = styled.div`
    
  `
  export const StyledText = styled.div`
    
  `
  export const StyledLogo = styled.div`
  display: flex;
    align-items: center;
    margin-top: 20px;
    justify-content: center;
    
  `